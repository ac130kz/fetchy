# This file was autogenerated by uv via the following command:
#    uv pip compile pyproject.toml --extra test -o requirements.test.txt
annotated-types==0.7.0
    # via pydantic
anyio==4.6.2.post1
    # via
    #   fetchy (pyproject.toml)
    #   httpx
    #   httpx-ws
attrs==24.2.0
    # via hypothesis
brotli==1.1.0
    # via httpx
certifi==2024.8.30
    # via
    #   httpcore
    #   httpx
h11==0.14.0
    # via
    #   httpcore
    #   wsproto
h2==4.1.0
    # via httpx
hpack==4.0.0
    # via h2
httpcore==1.0.6
    # via
    #   httpx
    #   httpx-ws
httpx==0.27.2
    # via
    #   fetchy (pyproject.toml)
    #   httpx-ws
httpx-ws==0.6.2
    # via fetchy (pyproject.toml)
hyperframe==6.0.1
    # via h2
hypothesis==6.116.0
    # via fetchy (pyproject.toml)
idna==3.10
    # via
    #   anyio
    #   httpx
iniconfig==2.0.0
    # via pytest
loguru==0.7.2
    # via fetchy (pyproject.toml)
packaging==24.1
    # via pytest
pluggy==1.5.0
    # via pytest
pydantic==2.9.2
    # via fetchy (pyproject.toml)
pydantic-core==2.23.4
    # via pydantic
pytest==8.3.3
    # via fetchy (pyproject.toml)
returns==0.23.0
    # via fetchy (pyproject.toml)
sniffio==1.3.1
    # via
    #   anyio
    #   httpx
sortedcontainers==2.4.0
    # via hypothesis
typing-extensions==4.12.2
    # via
    #   pydantic
    #   pydantic-core
    #   returns
uvloop==0.21.0
    # via fetchy (pyproject.toml)
wsproto==1.2.0
    # via httpx-ws
zstandard==0.23.0
    # via httpx
