import random

"""
https://aws.amazon.com/blogs/architecture/exponential-backoff-and-jitter/
https://github.com/aws-samples/aws-arch-backoff-simulator/blob/master/src/backoff_simulator.py
https://aws.amazon.com/builders-library/timeouts-retries-and-backoff-with-jitter/
"""


class Backoff:
    def __init__(self, base: float, cap: float):
        self.base = base
        self.cap = cap

    def backoff(self, _n: int) -> float:
        return 0

    def expo(self, n) -> float:
        return min(self.cap, (2**n) * self.base)


class NoBackoff(Backoff):
    def __init__(self):
        super().__init__(0.0, 0.0)

    def backoff(self, _n):
        return 0


class ExpoBackoff(Backoff):
    def backoff(self, n):
        return self.expo(n)


class ExpoBackoffEqualJitter(Backoff):
    def backoff(self, n):
        v = self.expo(n)
        return v / 2 + random.uniform(0, v / 2)  # noqa: S311


class ExpoBackoffFullJitter(Backoff):
    def backoff(self, n):
        v = self.expo(n)
        return random.uniform(0, v)  # noqa: S311


class ExpoBackoffDecorr(Backoff):
    def __init__(self, base, cap):
        Backoff.__init__(self, base, cap)
        self.sleep = self.base

    def backoff(self, _n):
        self.sleep = min(self.cap, random.uniform(self.base, self.sleep * 3))  # noqa: S311
        return self.sleep
