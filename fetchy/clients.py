from collections.abc import AsyncGenerator
from contextlib import asynccontextmanager

import httpx
import httpx_ws

# global cache of ssl context
# https://github.com/encode/httpx/issues/838
#
_ssl_context = httpx.create_ssl_context(http2=True)


@asynccontextmanager
async def http_client(retries: int = 0) -> AsyncGenerator[httpx.AsyncClient, None]:
    """
    Reusable http client component.
    """
    # caching ssl context and enabling http support
    transport = httpx.AsyncHTTPTransport(
        verify=_ssl_context,
        retries=retries,
        http2=True,
    )
    async with httpx.AsyncClient(transport=transport) as client:
        yield client


@asynccontextmanager
async def ws_client(
    url: str,
    max_message_size_bytes: int = 131072,
    queue_size: int = 1024,
    keepalive_ping_interval_seconds: int = 25,
    keepalive_ping_timeout_seconds: int = 5,
) -> AsyncGenerator[httpx_ws.AsyncWebSocketSession, None]:
    """
    Reusable websocket client component.
    """
    async with (
        http_client() as client,
        httpx_ws.aconnect_ws(
            url,
            client,
            max_message_size_bytes=max_message_size_bytes,
            queue_size=queue_size,
            keepalive_ping_interval_seconds=keepalive_ping_interval_seconds,
            keepalive_ping_timeout_seconds=keepalive_ping_timeout_seconds,
        ) as ws,
    ):
        yield ws
