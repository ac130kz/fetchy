import asyncio
import time
from abc import ABC
from collections.abc import Awaitable, Callable, Mapping, MutableMapping, Sequence
from enum import StrEnum, auto
from typing import NamedTuple

from httpx import AsyncClient, HTTPError, HTTPStatusError, codes
from httpx._client import USE_CLIENT_DEFAULT, UseClientDefault
from httpx._models import Response
from httpx._types import AuthTypes, PrimitiveData, RequestData
from loguru import logger
from pydantic import BaseModel, ConfigDict
from returns.future import future_safe

from fetchy import backoff
from fetchy.backoff import Backoff
from fetchy.clients import http_client


class RequestType(StrEnum):
    GET = auto()
    POST = auto()
    PUT = auto()
    PATCH = auto()
    DELETE = auto()


class BaseModelPydantic(BaseModel, ABC):
    model_config = ConfigDict(
        # required for enum value output
        use_enum_values=True,
    )


class BaseResponse(BaseModelPydantic, ABC): ...


class BaseRequestParams(NamedTuple):
    # required
    url: str  # NOTE: do not use AnyHttpUrl https://github.com/pydantic/pydantic/issues/7186
    log_str: str  # A human readable string to print into logs

    # optional
    request_type: RequestType = RequestType.GET
    auth: AuthTypes | UseClientDefault = USE_CLIENT_DEFAULT
    data: RequestData | BaseModelPydantic | None = None  # Passed into query
    params: Mapping[str, PrimitiveData | Sequence[PrimitiveData]] | None = None
    # unfortunately, RequestContent also requires AsyncIterable to be implemented
    content: str | bytes | BaseModelPydantic | None = None  # Passed into body
    headers: MutableMapping[str, str] | None = None
    timeout: float = 5.0  # Timeout value in seconds
    retries: int = 0
    backoff: Backoff = backoff.NoBackoff()


class Fetchy:
    """
    A safe Pydantic based wrapper around the http client.
    """

    @classmethod
    @future_safe
    async def request[R: BaseResponse](
        cls,
        params: BaseRequestParams,
        response_type: type[R],  # a hack to allow type instantiation since we don't have static typing
        by_alias: bool = False,  # noqa: FBT001, FBT002
    ) -> R:
        start = time.monotonic()
        retries = 0

        logger.info(f"[fetchy] request {params.request_type} -> {params.url}")

        # the loop is only used for retrying the request
        while True:
            async with http_client() as session:
                try:
                    response = await cls._handle_request(session, params, by_alias)

                    # preemptively check for success
                    response.raise_for_status()

                    # parse and instanciate the model
                    logger.trace(response.content)
                    response_model = response_type.model_validate_json(response.content)
                    logger.trace(response_model)
                    return response_model  # noqa: TRY300
                except HTTPStatusError as http_response_exc:
                    msg = f"[fetchy] error: {http_response_exc.response.status_code} -> {params.url}"
                    logger.error(msg)
                    logger.error(http_response_exc.response.content)

                    if retries < params.retries:
                        # retry loop: wait until the rate limit is over using the backoff, client is recreated just in case
                        if http_response_exc.response.status_code == codes.TOO_MANY_REQUESTS:
                            required_sleep = params.backoff.backoff(retries)
                            logger.debug(
                                f"[fetchy] retrying {params.log_str} - [{retries + 1}/{params.retries}] in {required_sleep:.2f}s"
                            )
                            await asyncio.sleep(required_sleep)
                            retries += 1

                        continue

                    raise
                except HTTPError as http_response_error_exc:
                    msg = f"[fetchy] error: {http_response_error_exc.with_traceback}"
                    logger.error(msg)
                    raise
                except ValueError:
                    msg = f"[fetchy] validation error: {params.log_str} -> {params.url}"
                    logger.error(msg)
                    raise
                finally:
                    logger.info(f"[fetchy] {params.log_str} - took: {time.monotonic() - start:.2f}s")

    @staticmethod
    async def _handle_request(
        session: AsyncClient,
        params: BaseRequestParams,
        by_alias: bool,  # noqa: FBT001
    ) -> Response:
        request_method: Callable[..., Awaitable[Response]] = {
            RequestType.GET: session.get,
            RequestType.POST: session.post,
            RequestType.PUT: session.put,
            RequestType.PATCH: session.patch,
            RequestType.DELETE: session.delete,
        }[params.request_type]

        match params.request_type:
            case RequestType.POST | RequestType.PUT | RequestType.PATCH:
                headers = {} if params.headers is None else params.headers

                if isinstance(params.content, BaseModelPydantic):
                    headers["content-type"] = "application/json"
                    content = params.content.model_dump_json(by_alias=by_alias)
                else:
                    content = params.content

                if isinstance(params.data, BaseModelPydantic):
                    data = params.data.model_dump(by_alias=by_alias)
                else:
                    data = params.data

                return await request_method(
                    params.url,
                    auth=params.auth,
                    headers=headers,
                    params=params.params,
                    timeout=params.timeout,
                    data=data,
                    content=content,
                )
            case RequestType.GET | RequestType.DELETE:
                return await request_method(
                    params.url,
                    auth=params.auth,
                    headers=params.headers,
                    params=params.params,
                    timeout=params.timeout,
                )
