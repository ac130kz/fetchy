## Fetchy

Prebaked HTTP and Websocket clients, and a convenient wrapper with utilities for fast and safe Pydantic JSON based API integration. For examples look at the tests.

Features:

- Clients: http (`fechy.clients.http_client`), websocket (`fechy.clients.ws_client`).
- Wrapper: `fetchy.http.Fetchy`, and utilities: `fetchy.http.{BaseModelPydantic, BaseRequestParams, BaseResponse}`.

## TODOs

- check whether .stream with 16 KB chunks, which will utilize HTTP2's enchanced multiplexing, is faster than the standard approach
- utilize httpx's internal per server connection pooling (maybe with \*args or overrides)
- possibly get rid of loguru, write directly to the standard sink
- make websocket an optional dependency
- replace Pydantic with a pluggable transport via a protocol (also to use xml and other filetypes in the future)
- possibly make an aiohttp backend, because httpx has a ton of performance issues at the time