from typing import Literal

import pytest
from httpx import HTTPStatusError, codes
from loguru import logger
from returns.io import IOFailure, IOSuccess
from returns.result import Failure, Success

from fetchy.http import BaseRequestParams, BaseResponse, Fetchy


class FailureResponse(BaseResponse):
    message: Literal["Hello_Peter"]


class FailureResponseNotFound(BaseResponse):
    status: Literal["404"]


class FailureResponseTooMany(BaseResponse):
    status: Literal["429"]


@pytest.mark.anyio
async def test_failure_not_found():
    result_response = await Fetchy.request(
        BaseRequestParams(
            url="https://dummyjson.com/http/404/Hello_Peter",
            log_str="[todo] request",
        ),
        FailureResponseNotFound,
    )
    match result_response:
        case IOSuccess(Success(_result)):
            raise  # noqa: PLE0704
        case IOFailure(Failure(exc)):
            logger.error(f"{exc}")
            assert isinstance(exc, HTTPStatusError)  # noqa: S101
            assert exc.response.status_code == codes.NOT_FOUND  # noqa: S101


@pytest.mark.anyio
async def test_failure_too_many():
    result_response = await Fetchy.request(
        BaseRequestParams(
            url="https://dummyjson.com/http/429/Hello_Peter",
            log_str="[todo] request",
            retries=3,
        ),
        FailureResponseTooMany,
    )
    match result_response:
        case IOSuccess(Success(_result)):
            raise  # noqa: PLE0704
        case IOFailure(Failure(exc)):
            logger.error(f"{exc}")
            assert isinstance(exc, HTTPStatusError)  # noqa: S101
            assert exc.response.status_code == codes.TOO_MANY_REQUESTS  # noqa: S101


@pytest.mark.anyio
async def test_failure_not_found_retry():
    result_response = await Fetchy.request(
        BaseRequestParams(
            url="https://dummyjson.com/http/404/Hello_Peter",
            log_str="[todo] request",
        ),
        FailureResponseNotFound,
    )
    match result_response:
        case IOSuccess(Success(_result)):
            raise  # noqa: PLE0704
        case IOFailure(Failure(exc)):
            logger.error(f"{exc}")
            assert isinstance(exc, HTTPStatusError)  # noqa: S101
            assert exc.response.status_code == codes.NOT_FOUND  # noqa: S101
