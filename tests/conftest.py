import asyncio

import pytest
import uvloop


@pytest.fixture(
    scope="session",
    params=[
        pytest.param(("asyncio", {"use_uvloop": True}), id="asyncio+uvloop"),
    ],
)
def anyio_backend(request):
    """
    Setup io backend options.
    """
    return request.param


@pytest.fixture(scope="session", autouse=True)
def run_tests_in_uvloop():
    """
    Run tests within uvloop's run loop.
    """
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
