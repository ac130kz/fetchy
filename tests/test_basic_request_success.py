import pytest
from loguru import logger
from pydantic import Field
from returns.io import IOFailure, IOSuccess
from returns.result import Failure, Success

from fetchy.http import BaseModelPydantic, BaseRequestParams, BaseResponse, Fetchy


class TodoResponse(BaseResponse):
    class Todo(BaseModelPydantic):
        todo_id: int = Field(..., validation_alias="id")
        todo: str
        completed: bool
        user_id: int = Field(..., validation_alias="userId")

    todos: list[Todo]
    total: int
    skip: int
    limit: int


@pytest.mark.anyio
async def test_success():
    result_response = await Fetchy.request(
        BaseRequestParams(
            url="https://dummyjson.com/todos",
            log_str="[todo] request",
        ),
        TodoResponse,
    )
    match result_response:
        case IOSuccess(Success(_result)):
            ...
        case IOFailure(Failure(exc)):
            logger.error(f"{exc}")
            raise  # noqa: PLE0704
